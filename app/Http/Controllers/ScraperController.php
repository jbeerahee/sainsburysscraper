<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Exception;
use App\Helpers\ScraperHelper;

/**
 * Class ScraperController
 * @package App\Http\Controllers
 * created Jamil Beerahee
 */
class ScraperController extends Controller
{

    /**
     * Url to scrape
     *
     * @var string $url
     */
    private $url;

    /**
     * Scrapes the url from the get request parameter.
     * Returns in json format each of the links text, size of html for the corresponding link, unit-price per link
     * Total of all unit prices
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {

        try {
            $scraperHelper = new ScraperHelper();

            $url = $request->input('url');

            $scrapedObj = $scraperHelper->getScrapedHtml($url);

            $scrapedLinkText = $scraperHelper->getLinkTextFromHtml($scrapedObj);

            if (empty($scrapedLinkText)) {
                throw new Exception('No Link text found for url:'. $this->url);
            }

            $prices = $scraperHelper->getPricesFromHtml($scrapedObj);

            $count = 0;
            $total = 0;

            foreach ($scrapedLinkText as $linkTextItem) {

                $scrapeData[$count]['title'] = $linkTextItem;
                $html = $scraperHelper->getHtmlFromFollowedLink($scrapedObj, $linkTextItem);
                $scrapeData[$count]['size'] = $scraperHelper->getHtmlSizeInKb($html);
                $scrapeData[$count]['unit_price'] = $prices[$count];
                $total += $prices[$count];

                $count++;
            }

            $scrape['results'] = $scrapeData;
            $scrape['total'] = $total;

            return $scrape;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

}
