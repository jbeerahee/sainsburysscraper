<?php

namespace app\Helpers;

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class ScraperHelper
 * @package app\Helpers
 * created by Jamil Beerahee
 */
class ScraperHelper
{
    const ASSETS_REG_EXP = '/<script\b[^>]*>(.*?)<\/script>/is';

    /**
     * @var object $goutteClient
     */
    private $goutteClient;

    /**
     * Initialise variables
     */
    public function init()
    {
        $this->goutteClient = new Client();
    }

    /**
     * Scrapes the page url passed through
     *
     * @param string $url
     */
    public function getScrapedHtml($url)
    {
        $this->init();
        $scrapeObj = $this->goutteClient->request('GET', $url);

        return $scrapeObj;
    }

    /**
     * Gets the prices of each link from the scrape html
     *
     * @param object $scrapeObj
     * @return array
     */
    public function getPricesFromHtml($scrapeObj)
    {
        $prices = $scrapeObj->filter('div.pricing > p.pricePerUnit')->each(function (Crawler $node, $i) {
            return preg_replace("/[^0-9\.]/", '', trim($node->text()));
        });

        return $prices;
    }

    /**
     * Gets the link text for each link from the scraped html
     *
     * @param $scrapeObj
     * @return array
     */
    public function getLinkTextFromHtml($scrapeObj)
    {
        $linkText = $scrapeObj->filter('#productLister > ul > li > div.product > div > div.productInfoWrapper > div > h3 > a[href]')->each(function ($node) {
            return trim($node->text());
        });

        return $linkText;
    }

    /**
     * Follows the link passed and strips the html of assets and returns the html
     *
     * @param object $scrapeObj
     * @param string $scrapedLinkText
     * @return string $strippedHtml
     */
    public function getHtmlFromFollowedLink($scrapeObj, $scrapedLinkText)
    {
        $link = $scrapeObj->selectLink($scrapedLinkText)->link();
        $crawler = $this->goutteClient->click($link);
        $html = $crawler->html();
        $strippedHtml = $this->stripHtmlOfAssets($html);

        return $strippedHtml;
    }

    /**
     * Strips the string of assets via reg expression
     *
     * @param string $html
     * @return mixed
     */
    public function stripHtmlOfAssets($html)
    {
        return preg_replace(self::ASSETS_REG_EXP, "", $html);
    }

    /**
     * Gets the size of the string in kb
     *
     * @param $html
     * @return string
     */
    public function getHtmlSizeInKb($html)
    {
        return number_format(strlen($html) / 1024);

    }

}