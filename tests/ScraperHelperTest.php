<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Helpers\ScraperHelper;
use Symfony\Component\DomCrawler\Crawler;
use Goutte\Client;

/**
 * Class ScraperHelperTest
 * created by Jamil Beerahee
 */
class ScraperHelperTest extends TestCase
{

    /**
     * @var object $scraperHelper
     */
    public $scraperHelper;

    /**
     * @var string $url
     */
    public $url;

    /**
     * @var string $mockHtml
     */
    public $mockHtml;

    /**
     * @var string $mockJavascript
     */
    public $mockJavascript;

    /**
     * Setup the test environment.
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        if (!$this->app) {
            $this->refreshApplication();
        }
        $this->url = 'http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html';
        $this->scraperHelper = new ScraperHelper();

    }

    /**
     * Clean up the testing environment before the next test.
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        if ($this->app) {
            $this->app->flush();
        }

    }

    /**
     * Tests the getPricesFromHtml function checks the mock prices matches up
     */
    public function testGetPricesFromHtml()
    {
        $mockedPrices = [
            0 => "3.50",
            1 => "1.50",
            2 => "1.80",
            3 => "3.20",
            4 => "1.50",
            5 => "1.80",
            6 => "1.80"
        ];

        $scrapeObj = $this->scraperHelper->getScrapedHtml($this->url);
        $prices = $this->scraperHelper->getPricesFromHtml($scrapeObj);
        $this->assertEquals($prices, $mockedPrices);
    }

    /**
     * Tests all the link text functionality and matches up with mock data
     */
    public function testGetLinkTextFromHtml()
    {
        $mockLinkedText = [
            0 => "Sainsbury's Apricot Ripe & Ready x5",
            1 => "Sainsbury's Avocado Ripe & Ready XL Loose 300g",
            2 => "Sainsbury's Avocado, Ripe & Ready x2",
            3 => "Sainsbury's Avocados, Ripe & Ready x4",
            4 => "Sainsbury's Conference Pears, Ripe & Ready x4 (minimum)",
            5 => "Sainsbury's Golden Kiwi x4",
            6 => "Sainsbury's Kiwi Fruit, Ripe & Ready x4"
        ];

        $scrapeObj = $this->scraperHelper->getScrapedHtml($this->url);
        $linkText = $this->scraperHelper->getLinkTextFromHtml($scrapeObj);

        $this->assertEquals($linkText, $mockLinkedText);
    }

    /**
     * Tests the first link Sainsbury's Apricot Ripe & Ready x5
     */
    public function testGetHtmlFromFollowedLink()
    {

        $scrapeObj = $this->scraperHelper->getScrapedHtml($this->url);
        $linkText = $this->scraperHelper->getLinkTextFromHtml($scrapeObj);
        $html = $this->scraperHelper->getHtmlFromFollowedLink($scrapeObj, $linkText[0]);

        $this->assertContains('7572754', $html);
        $this->assertContains("<h1>Sainsbury's Apricot Ripe &amp; Ready x5</h1>", $html);
    }

    /**
     * Tests the stripHtmlOfAssets function and checks the javascript is missing
     */
    public function testStripHtmlOfAssets()
    {
        $this->mockData();
        $strippedHtml = $this->scraperHelper->stripHtmlOfAssets($this->mockHtml);
        $this->assertNotContains($this->mockJavascript, $strippedHtml);
    }

    /**
     * Tests the getHtmlSizeInKb function for the first link which should equal to 31kb
     */
    public function testGetHtmlSizeInKb()
    {
        $scrapeObj = $this->scraperHelper->getScrapedHtml($this->url);
        $linkText = $this->scraperHelper->getLinkTextFromHtml($scrapeObj);
        $html = $this->scraperHelper->getHtmlFromFollowedLink($scrapeObj, $linkText[0]);
        $size = $this->scraperHelper->getHtmlSizeInKb($html);

        $this->assertEquals(31, $size);
    }

    /**
     * Mock data html and javascript
     */
    public function mockData()
    {
        $this->mockHtml = "
        <html>
        <head>
        <body>
        <script type=\"text/javascript\">
            var brightTagStAccount = 'sp0XdVN';
        </script>
        </head>
        </body>
        </html>
        ";
        $this->mockJavascript = "<script type=\"text/javascript\">
            var brightTagStAccount = 'sp0XdVN';
        </script>";
    }


}
