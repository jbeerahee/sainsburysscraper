<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Http\Controllers\ScraperController;
use App\Http\Requests;


/**
 * Class ScraperControllerTest
 * created by Jamil Beerahee
 */
class ScraperControllerTest extends TestCase
{
    /**
     * @var object $scraperController
     */
    public $scraperController;

    /**
     * @var string $url
     */
    public $url;

    /**
     * Setup the test environment.
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        if (!$this->app) {
            $this->refreshApplication();
        }
        $this->url = 'http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html';
        $this->scraperController = new ScraperController();

    }

    /**
     * Clean up the testing environment before the next test.
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        if ($this->app) {
            $this->app->flush();
        }

    }

    /**
     * Tests an unsuccessful call
     *
     * @return void
     */
    public function testScrapeControllerFail()
    {
        $this->visit('/scrape?url=')
            ->see('No Link text found for url:');
    }

    /**
     * Tests a successful call
     *
     * @return void
     */
    public function testScrapeControllerSuccess()
    {
        $this->visit('/scrape?url='.$this->url)
            ->dontSee('No Link text found for url:');
    }
}
