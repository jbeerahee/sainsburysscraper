# Sainsburys Page Scraper

A php laravel application that scrapes a Sainsburys grocery page and returns a json.

### Prerequisites

- Install composer
- The app is using Laravel 5.3 please see https://laravel.com/docs/5.3/ for installation of laravel.
- I used Homestead for a quick installation of the technologies needed to run the app ubuntu, php7, phpunit, https://laravel.com/docs/5.3/homestead


### Installing

- git clone https://jbeerahee@bitbucket.org/jbeerahee/sainsburysscraper.git to clone the sainsburysscraper repository.
- run 'composer update' should update and install all the required dependancies needed for the scraper
- Setup your local virtual host I used sainsburysscraper.app

##Usage
- In your browser run the following url: http://sainsburysscraper.app/scrape/?url=http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html

## Running the tests
- Testing uses phpunit, to run all tests run the following in the project root:
- "phpunit"

- For individual tests 
- "phpunit --filter ScraperHelperTest" 
- "phpunit --filter ScraperControllerTest" 

You should see 7 tests completed successfully.

## Authors
* **Jamil Beerahee**